/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-12-01
 */
#include "GameMap.h"
GameMap::GameMap() {
  mapGen = new MapGen();
  CreateMap();
}

GameMap::~GameMap() {
  DestoryMap();
  delete mapGen;
}
void GameMap::CreateMap() {
  //gameMap[5*4];
  //adding rooms to map
  for (int i = 0; i < WIDTH * HEIGHT; i++) {
    switch (i) {
      //KITCHEN
      case KITCHEN_CORDS:
        gameMap.push_back(mapGen->CreateMapLocation(KITCHEN));
        break;
        //TOPAIRLOCK
      case TOPAIRLOCK_CORDS:
        gameMap.push_back(mapGen->CreateMapLocation(TOPAIRLOCK));
        break;
        //CREWQUATERS
      case CREWQUATERS_CORDS:
        gameMap.push_back(mapGen->CreateMapLocation(CREWQUATERS));
        break;
        //COCKPIT
      case COCKPIT_CORDS:
        gameMap.push_back(mapGen->CreateMapLocation(COCKPIT));
        break;
        //ELEVATOR
      case ELEVATOR_CORDS:
        gameMap.push_back(mapGen->CreateMapLocation(ELEVATOR));
        break;
        //BRIDGE
      case BRIDGE_CORDS:
        gameMap.push_back(mapGen->CreateMapLocation(BRIDGE));
        break;
        //ENGINEROOM
      case ENGINEROOM_CORDS:
        gameMap.push_back(mapGen->CreateMapLocation(ENGINEROOM));
        break;
        //FOODSTORAGE
      case FOODSTORAGE_CORDS:
        gameMap.push_back(mapGen->CreateMapLocation(FOODSTORAGE));
        break;
        //WHEELWELL1
      case WHEELWELL1_CORDS:
        gameMap.push_back(mapGen->CreateMapLocation(WHEELWELL1));
        break;
        //BAGGAGESTORAGE
      case BAGGAGESTORAGE_CORDS:
        gameMap.push_back(mapGen->CreateMapLocation(BAGGAGESTORAGE));
        break;
        //WHEELWELL2
      case WHEELWELL2_CORDS:
        gameMap.push_back(mapGen->CreateMapLocation(WHEELWELL2));
        break;
        //BATHROOM
      case BATHROOM_CORDS:
        gameMap.push_back(mapGen->CreateMapLocation(BATHROOM));
        break;
        //INVALID
      default:
        gameMap.push_back(nullptr);
    }
  }
}

void GameMap::DestoryMap() {
  for (int i = 0; i < WIDTH * HEIGHT; i++) {
    delete gameMap[i];
  }
  gameMap.clear();
}

Environment* GameMap::GetRoom(Coordinate *cords) {
  return gameMap[cords->GetX() ];
}
Environment* GameMap::GetRoom(int x) {
  return gameMap[x];
}
Inventory* GameMap::GetRoomContents(Coordinate *cords) {
  return gameMap[cords->GetX()]->GetInventory();
}
/*
Environment* GameMap::GetRoom(int i) {
  return gameMap[i];
}
*/
