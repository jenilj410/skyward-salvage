/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-12-01
 */
#include <MapGen.h>
/*
 * MapGen implementation
 */
MapGen::MapGen() {
}
MapGen::~MapGen() {
}
Environment* MapGen::CreateMapLocation(RoomType type) {
  switch (type) {
    case RoomType::BRIDGE:
      return new Bridge();
      break;
    case RoomType::COCKPIT:
      return new CockPit();
      break;
    case RoomType::ENGINEROOM:
      return new EngineRoom2();
      break;
    case RoomType::FOODSTORAGE:
      return new FoodStorage();
      break;
    case RoomType::ELEVATOR:
      return new Elevator();
      break;
    case RoomType::CREWQUATERS:
      return new CrewQuaters();
      break;
    case RoomType::KITCHEN:
      return new Kitchen();
      break;
    case RoomType::BATHROOM:
      return new Bathroom();
      break;
    case RoomType::BAGGAGESTORAGE:
      return new BaggageStorage1();
      break;
    case RoomType::WHEELWELL1:
      return new WheelWell1();
      break;
    case RoomType::WHEELWELL2:
      return new WheelWell2();
      break;
    case RoomType::TOPAIRLOCK:
      return new TopAirLock();
      break;
  }
  return nullptr;
}
