/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-12-01
 */
#include "QuestItems.h"
/*
 * CockpitParts implementation
 */
CockpitParts::CockpitParts() {
  name = "CockPitParts";
}
CockpitParts::~CockpitParts() {
}
void CockpitParts::GetDescription() {
  std::cout << "> " << name << std::endl;
  std::cout << "> The Cockpit parts, prized for you!!! " << std::endl;
}
/*
 * Medal implementation
 */
Medal::Medal() {
  name = "Medal";
}
Medal::~Medal() {
}

void Medal::GetDescription() {
  std::cout << "> " << name << std::endl;
  std::cout << "> The Pilots Medal, crafted from the finest " <<
  "metals and polished to a gleaming shine." << std::endl;
}
