/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-12-01
 */
#include <Pilot.h>
/*
 * Pilot implementation
 */
Pilot::Pilot() {
  ItemStorage itemFact;
  name = "Pilot";
  item = itemFact.CreateItem(MEDAL);
}
Pilot::~Pilot() {
  delete item;
}
void Pilot::GetDialog() {
  std::cout << "[Pilot]:" << std::endl << "You there! Listen up" << std::endl
      << std::endl << "[Quest]: I need you to get a [Medal] for me."
      << std::endl << "From my calculations it should be "
      << "hidden somewhere [EngineRoom] or in [BaggageRoom]" << std::endl
      << "from where we've anchored." << std::endl << std::endl
      << "Now, get yourself out there and be useful!" << std::endl
      << "And by the way, make sure you got yourself some [Airtanks]"
      << std::endl
      << "Before you go out into dangerous air pressure."
      << std::endl
      << "GAHAHAHAHA!!" << std::endl;
}
void Pilot::FinsihQuest(PlayerData* player) {
  if (player->GetInventory()->HasItem(item)) {
    std::cout << "[Pilot]:" << std::endl << "Haha! Thank you my lad!"
        << std::endl << "You actually got the [Medal] I asked for!" << std::endl
        << "I underestimated you!" << std::endl
        << "Seeing how useful you are, go help whoever "
        << "else needs it around the [Plane]." << std::endl << std::endl;
    player->GetInventory()->RemoveItem(item->GetName());
    player->addScore();
    delete item;
    item = nullptr;
  } else {
    std::cout << "[Pilot]:" << std::endl << "Quite a jokester here!"
        << std::endl << "Come back to me when actually got the booty I need"
        << std::endl << std::endl;
  }
}
