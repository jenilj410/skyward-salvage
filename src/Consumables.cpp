/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-12-01
 */
#include "Consumables.h"
/*
 * AirTank implementation
 */
AirTank::AirTank() {
  name = "Airtank";
}
AirTank::~AirTank() {
}

void AirTank::GetDescription() {
  std::cout << "> " << name << std::endl;
  std::cout << "> This air tank ensures you'll have enough air to " <<
  "explore even the darkest area of " <<
  " baggage storage and wheel well , engine room " <<
  " depths, granting you the freedom to " <<
  "discover the mysteries that lie beneath " <<
  " the Wheel Wells and Engine Room." << std::endl;
}
/*
 * MedKit implementation
 */
MedKit::MedKit() {
  name = "Medkit";
}
MedKit::~MedKit() {
}

void MedKit::GetDescription() {
  std::cout << "> " << name << std::endl;
  std::cout << "> Packed with essential supplies to " <<
  "treat injuries and prevent infections, "
  << "it's a lifesaver in the most dire of situations." << std::endl;
}
