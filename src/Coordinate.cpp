/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-12-01
 */
#include "Coordinate.h"
/*
 * Coordinate implementation
 */
Coordinate::Coordinate(int x) {
  cord = x;
}
Coordinate::~Coordinate() {
}
void Coordinate::OverRideCord(int x) {
  cord = x;
}

void Coordinate::UpdateX(int x) {
  cord = x;
}
int Coordinate::GetX() {
  return cord;
}
