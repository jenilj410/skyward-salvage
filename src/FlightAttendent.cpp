/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-12-01
 */
#include <FlightAttendent.h>
/*
 * FlightAttendent implementation
 */
FlightAttendent::FlightAttendent() {
  ItemStorage itemFact;
  name = "FlightAttendent";
  item = itemFact.CreateItem(COCKPITPARTS);
}
FlightAttendent::~FlightAttendent() {
  delete item;
}

void FlightAttendent::GetDialog() {
  std::cout << "[FlightAttendent]:" << std::endl << "Hey There!" << std::endl
      << "Quite the pickle we've got ourselves in aye?" << std::endl
      << std::endl << "[Quest]: Could you be a pal "
      << "and find me a [CockPit Part]?" << std::endl
      << "From what've been told there should be "
      << "some living somewhere [UnderGround] around here" << std::endl
      << "Before you go, make sure you got yourself some [Airtanks]"
      << std::endl << "Before you go UnderGround." << std::endl << std::endl;
}
void FlightAttendent::FinsihQuest(PlayerData* player) {
  if (player->GetInventory()->HasItem(item)) {
    std::cout << "[FlightAttendent]:" << std::endl
    << "Aha! Thank you!" << std::endl
        << "Quite the specimen you got there." << std::endl
        << "I'll take it of your hands here." << std::endl
        << "If you got any more free time I'm sure there's other stuff"
        << " that needs helping around the sub." << std::endl << std::endl;
    player->GetInventory()->RemoveItem(item->GetName());
    player->addScore();
    delete item;
    item = nullptr;
  } else {
    std::cout << "[FlightAttendent]:" << std::endl
        << "Doesn't seem you have what I want there buddy" << std::endl
        << "Try looking around more and come back to me "
        << "when you have a [CockPit Part]." << std::endl;
  }
}
