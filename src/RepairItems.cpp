/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-12-01
 */
#include<RepairItems.h>
/*
 * Tools implementation
 */
Tools::Tools() {
  name = "Tools";
}
Tools::~Tools() {
}

void Tools::GetDescription() {
  std::cout << "> " << name << std::endl;
  std::cout << "> This sturdy bag of tools contains everything " <<
  "a skilled craftsman needs to tackle any project." << std::endl;
}
/*
 * PlaneParts implementation
 */
PlaneParts::PlaneParts() {
  name = "PlaneParts";
}
PlaneParts::~PlaneParts() {
}

void PlaneParts::GetDescription() {
  std::cout << "> " << name << std::endl;
  std::cout << "> A set of metallic gauges and pistons used" <<
  " repair the the aircraft steering control system. " << std::endl;
}
/*
 * Patch implementation
 */
Patch::Patch() {
  name = "Patch";
}
Patch::~Patch() {
}

void Patch::GetDescription() {
  std::cout << "> " << name << std::endl;
  std::cout << "> A small and unassuming patch made " <<
  "of some rubbery material." << std::endl;
}
/*
 * EngineParts implementation
 */
EngineParts::EngineParts() {
  name = "EngineParts";
}
EngineParts::~EngineParts() {
}
void EngineParts::GetDescription() {
  std::cout << "> " << name << std::endl;
  std::cout << "> A collection of metal pieces and " <<
  "bolts used to repair the engine. " << std::endl;
}
