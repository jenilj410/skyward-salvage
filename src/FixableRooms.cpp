/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-12-01
 */
#include <FixableRooms.h>
/*
 * EngineRoom2 implementation
 */
EngineRoom2::EngineRoom2() {
  name = "EngineRoom2";
  inventory = new Inventory();
  requiredItems = new Inventory();
  SetContents();
}
EngineRoom2::~EngineRoom2() {
  delete requiredItems;
  delete inventory;
}
void EngineRoom2::SetContents() {
  requiredItems->AddItem(TOOLS, "Tools");
  requiredItems->AddItem(PLANEPARTS, "PlaneParts");
  requiredItems->AddItem(ENGINEPARTS, "EngineParts");
}

Inventory* EngineRoom2::GetInventory() {
  std::cout << "> There is nothing of value here " << std::endl
      << " nor anywhere to store items" << std::endl;
  return inventory;
}
Inventory* EngineRoom2::GetReqInventory() {
  return requiredItems;
  // return 0;
}
void EngineRoom2::GetInfo() {
  std::cout << "> Currently in the " << name << std::endl;
  std::cout << "> You notice that there is only one way to move" << std::endl
      << "> right." << std::endl << "> you also notice the engine is damage "
      << std::endl << "and can be repaired using: " << std::endl;
  requiredItems->ListContents();
}
void EngineRoom2::FixRoom(Inventory *inventory) {
  std::cout << "> You decide try and repair. " << std::endl;
  bool canFix = requiredItems->CheckReqItems(inventory);
  if (canFix) {
    std::cout << "> Using the items gather you manage to fix it" << std::endl;
    requiredItems->UseItems(inventory);
    fixed = true;
  }
}
void EngineRoom2::UpWall(PlayerData *player) {
  std::cout << "> You decide to start jumping and end up slipping, "
      << std::endl << "injuring your self" << std::endl;
  player->UpdateHealth(-10);
}
void EngineRoom2::DownWall(PlayerData *player) {
  std::cout << "> You begin to walk but slip on a puddle, " << std::endl
      << "injuring your self" << std::endl;
  player->UpdateHealth(-10);
}
void EngineRoom2::RightWall(PlayerData *player) {
  std::cout << "> You move right" << std::endl;
  player->GetCords()->UpdateX(7);
}
void EngineRoom2::LeftWall(PlayerData *player) {
  std::cout << "> You move left and run into a wall" << std::endl;
  player->UpdateHealth(-10);
}
/*
 * BaggageStorage2 implementation
 */
BaggageStorage2::BaggageStorage2() {
  name = "BaggageStorage2";
  inventory = new Inventory();
  requiredItems = new Inventory();
  SetContents();
}
BaggageStorage2::~BaggageStorage2() {
  delete inventory;
  delete requiredItems;
}
void BaggageStorage2::SetContents() {
  inventory->AddItem(AIRTANK, "Airtank");
  requiredItems->AddItem(TOOLS, "Tools");
  requiredItems->AddItem(PATCH, "Patch");
}

Inventory* BaggageStorage2::GetInventory() {
  return inventory;
}
Inventory* BaggageStorage2::GetReqInventory() {
  return requiredItems;
}
void BaggageStorage2::GetInfo() {
  std::cout << "> Currently in the " << name << std::endl;
  std::cout << "> You notice that there are 4 ways Out of this room"
      << std::endl
      << "> Left, Right, Up or Down into the open air."
      << std::endl;
  if (!fixed) {
    std::cout << "> If you wish to enter the Air, "
        << "you must first put on the Airtank Suit," << std::endl
        << " which is damaged which to fix needs:" << std::endl;
  }
  requiredItems->ListContents();
  std::cout << "> you also notice some Items of Value: " << name << std::endl;
  inventory->ListContents();
}
void BaggageStorage2::FixRoom(Inventory *inventory) {
  std::cout << "> You decide to try and repair the diving gear. " << std::endl;
  bool canFix = requiredItems->CheckReqItems(inventory);
  if (canFix) {
    std::cout << "> Using the items gather you manage to fix it" << std::endl;
    requiredItems->UseItems(inventory);
    fixed = true;
  }
}
void BaggageStorage2::UpWall(PlayerData *player) {
  std::cout << "> You enter the elevator and move up a floor, " << std::endl;
  player->GetCords()->UpdateX(4);
}
void BaggageStorage2::DownWall(PlayerData *player) {
  std::cout << "> you enter the air lock " << std::endl;
  if (fixed) {
    std::cout
        << "> With the fixed diving gear, and leave the ship into the sea "
        << std::endl;
    player->GetCords()->UpdateX(10);
  } else {
    std::cout << "> With the broken diving gear, and as the chamber "
        << "begins to decompres" << std::endl
        << "> You regret not fixing the gear before hand,"
        << " as water rushes into the chamber " << std::endl
        << " > Shattering the your visors glass, killing you" << std::endl;
    player->Die();
  }
}
void BaggageStorage2::RightWall(PlayerData *player) {
  std::cout << "> You move right" << std::endl;
  player->GetCords()->UpdateX(8);
}
void BaggageStorage2::LeftWall(PlayerData *player) {
  std::cout << "> You move left" << std::endl;
  player->GetCords()->UpdateX(6);
}

/*
 * WheelWell2 implementation
 */
WheelWell2::WheelWell2() {
  name = "WheelWell2";
  inventory = new Inventory();
  requiredItems = new Inventory();
  SetContents();
}
WheelWell2::~WheelWell2() {
  delete inventory;
  delete requiredItems;
}
void WheelWell2::SetContents() {
  inventory->AddItem(AIRTANK, "airtank");
  requiredItems->AddItem(TOOLS, "tools");
  requiredItems->AddItem(PATCH, "patch");
}

Inventory* WheelWell2::GetInventory() {
  return inventory;
}
Inventory* WheelWell2::GetReqInventory() {
  return requiredItems;
}
void WheelWell2::GetInfo() {
  std::cout << "> Currently in the " << name << std::endl;
  std::cout << "> You notice that there are 4 ways Out of this room"
      << std::endl
      << "> Left, Right, Up or Down into the open air."
      << std::endl;
  if (!fixed) {
    std::cout << "> If you wish to enter the Air, "
        << "you must first put on the Airtank Suit," << std::endl
        << " which is damaged which to fix needs:" << std::endl;
  }
  requiredItems->ListContents();
  std::cout << "> you also notice some Items of Value: " << name << std::endl;
  inventory->ListContents();
}
void WheelWell2::FixRoom(Inventory *inventory) {
  std::cout << "> You decide to try and repair the diving gear. " << std::endl;
  bool canFix = requiredItems->CheckReqItems(inventory);
  if (canFix) {
    std::cout << "> Using the items gather you manage to fix it" << std::endl;
    requiredItems->UseItems(inventory);
    fixed = true;
  }
}
void WheelWell2::UpWall(PlayerData *player) {
  std::cout << "> You enter the elevator and move up a floor, " << std::endl;
  player->GetCords()->UpdateX(4);
}
void WheelWell2::DownWall(PlayerData *player) {
  std::cout << "> you enter the wheel well in airlock " << std::endl;
  if (fixed) {
    std::cout
        << "> With the fixed diving gear, and leave the plane into the air "
        << std::endl;
    player->GetCords()->UpdateX(10);
  } else {
    std::cout << "> With the broken diving gear, and as the chamber "
        << "begins to decompres" << std::endl
        << "> You regret not fixing the gear before hand,"
        << " as water rushes into the chamber " << std::endl
        << " > Shattering the your visors glass, killing you" << std::endl;
    player->Die();
  }
}
void WheelWell2::RightWall(PlayerData *player) {
  std::cout << "> You move right" << std::endl;
  player->GetCords()->UpdateX(8);
}
void WheelWell2::LeftWall(PlayerData *player) {
  std::cout << "> You move left" << std::endl;
  player->GetCords()->UpdateX(6);
}
