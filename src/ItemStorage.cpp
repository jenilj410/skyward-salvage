/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-12-01
 */
#include <ItemStorage.h>
/*
 * ItemStorage implementation
 */
ItemStorage::ItemStorage() {
}
ItemStorage::~ItemStorage() {
}
Item* ItemStorage::CreateItem(ItemType type) {
  switch (type) {
    case ItemType::PLANEPARTS:
      return new PlaneParts();
      break;
    case ItemType::TOOLS:
      return new Tools();
      break;
    case ItemType::PATCH:
      return new Patch();
      break;
    case ItemType::ENGINEPARTS:
      return new EngineParts();
      break;
    case ItemType::COCKPITPARTS:
      return new CockpitParts();
      break;
    case ItemType::MEDKIT:
      return new MedKit();
      break;
    case ItemType::AIRTANK:
      return new AirTank();
      break;
    case ItemType::MEDAL:
      return new Medal();
      break;
  }
  return nullptr;
}
