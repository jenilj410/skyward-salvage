/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-12-01
 */
#include <Air.h>
/*
 * BaggageStorage1 implementation
 */
BaggageStorage1::BaggageStorage1() {
  name = "BaggageStorage1";
  inventory = new Inventory();
}
BaggageStorage1::~BaggageStorage1() {
  delete inventory;
}
void BaggageStorage1::GetInfo() {
  std::cout << "> Currently " << name << std::endl;
  std::cout << "> You notice that there are 3 ways Out of this room"
      << std::endl << "> Left, Right, Up into the plane." << std::endl
      << "> there also seems to be nothing of interest here" << std::endl;
}
void BaggageStorage1::UpWall(PlayerData *player) {
  std::cout << "> You enter the elevator and move up a floor, " << std::endl;
  player->UpdateOxygen(-10);
  player->GetCords()->UpdateX(7);
}
void BaggageStorage1::DownWall(PlayerData *player) {
  std::cout << "> You decide to attempt to dive to go through the ground "
      << std::endl << "However unlike your beliefs it's solid not some liquid"
      << std::endl;
  player->UpdateOxygen(-10);
  player->UpdateHealth(-10);
}
void BaggageStorage1::RightWall(PlayerData *player) {
  std::cout << "> You move right" << std::endl;
  player->UpdateOxygen(-10);
  player->GetCords()->UpdateX(11);
}
void BaggageStorage1::LeftWall(PlayerData *player) {
  std::cout << "> You move left" << std::endl;
  player->UpdateOxygen(-10);
  player->GetCords()->UpdateX(9);
}
void BaggageStorage1::SetContents() {
  inventory->AddItem(TOOLS, "Tools");
  inventory->AddItem(PLANEPARTS, "PlaneParts");
  inventory->AddItem(ENGINEPARTS, "EngineParts");
  inventory->AddItem(MEDAL, "Medal");
}

/*
 * WheelWell1 implementation
 */
WheelWell1::WheelWell1() {
  name = "WheelWell1";
  inventory = new Inventory();
  SetContents();
}
WheelWell1::~WheelWell1() {
  delete inventory;
}
void WheelWell1::SetContents() {
  inventory->AddItem(TOOLS, "Tools");
  inventory->AddItem(PLANEPARTS, "PlaneParts");
  inventory->AddItem(ENGINEPARTS, "EngineParts");
  inventory->AddItem(MEDAL, "Medal");
}
void WheelWell1::GetInfo() {
  std::cout << "> Currently in a " << name << std::endl;
  std::cout << "> You notice that there is only one way to move" << std::endl
      << "> left or right." << std::endl << "> you also a ruined plane "
      << "Which might contain some loot." << std::endl;
}

void WheelWell1::UpWall(PlayerData *player) {
  std::cout << "> You decide to fly towards the hull of the sub hull. "
      << std::endl << "> and banging your head against it,Damaging Yourself"
      << std::endl;
  player->UpdateOxygen(-10);
  player->UpdateHealth(-10);
}

void WheelWell1::DownWall(PlayerData *player) {
  std::cout << "> You decide to fly towards the Sandy floor. " << std::endl
      << "> and end up hitting your head against the floor, Damaging Yourself."
      << std::endl;
  player->UpdateOxygen(-10);
  player->UpdateHealth(-10);
}

void WheelWell1::RightWall(PlayerData *player) {
  std::cout << "> You decide to move towards the wheelwell eastern wall,"
      << std::endl << "> and end up crashing into it, damaging your self"
      << std::endl;
  player->UpdateOxygen(-10);
  player->UpdateHealth(-10);
}

void WheelWell1::LeftWall(PlayerData *player) {
  std::cout << "> You move left" << std::endl;
  player->UpdateOxygen(-10);
  player->GetCords()->UpdateX(10);
}

/*
 * EngineRoom1 implementation
 */
EngineRoom1::EngineRoom1() {
  name = "EngineRoom1";
  inventory = new Inventory();
  SetContents();
}
EngineRoom1::~EngineRoom1() {
  delete inventory;
}
void EngineRoom1::SetContents() {
  inventory->AddItem(COCKPITPARTS, "CockPitParts");
}
void EngineRoom1::GetInfo() {
  std::cout << "> Currently in a " << name << std::endl;
  std::cout << "> You notice that there is only one way to move" << std::endl
      << "> right" << std::endl << "> you also a patch of cockpit parts "
      << std::endl;
}

void EngineRoom1::UpWall(PlayerData *player) {
  std::cout << "> You decide to fly towards the hull of the sub hull. "
      << std::endl << "> and banging your head against it, Damaging Yourself"
      << std::endl;
  player->UpdateOxygen(-10);
  player->UpdateHealth(-10);
}
void EngineRoom1::DownWall(PlayerData *player) {
  std::cout << "> You decide to fly towards the Sandy floor. " << std::endl
      << "> and end up hitting your head against the floor, Damaging Yourself."
      << std::endl;
  player->UpdateOxygen(-10);
  player->UpdateHealth(-10);
}

void EngineRoom1::RightWall(PlayerData *player) {
  std::cout << "> You move right" << std::endl;
  player->UpdateOxygen(-10);
  player->GetCords()->UpdateX(10);
}
void EngineRoom1::LeftWall(PlayerData *player) {
  std::cout << "> you decide to move left, to admire the beauty or the open air"
      << std::endl
      << "> but the beauty is soon interrupted as you find you self being"
      << std::endl << " >flown by air pressure" << std::endl;
  player->Die();
}
