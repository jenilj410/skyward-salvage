/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-12-01
 */
#include <NpcFact.h>
/*
 * NpcFact implementation
 */
NpcFact::NpcFact() {
}
NpcFact::~NpcFact() {
}
Npc* NpcFact::CreateNpc(NPCType type) {
    switch (type) {
        case NPCType::FLIGHTATTENDENT:
        return new FlightAttendent();
        break;
        case NPCType::PILOT:
        return new Pilot();
        break;
        case NPCType::MEDIC:
        return new Medic();
        break;
    }
    return nullptr;
}
