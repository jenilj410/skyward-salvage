/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-12-01
 */
#include "Rooms.h"
/*
 * Brdige implementation
 */
Bridge::Bridge() {
  name = "Bridge";
  inventory = new Inventory();
  SetContents();
}
Bridge::~Bridge() {
  delete inventory;
  delete npc;
}
void Bridge::SetContents() {
  NpcFact fact;
  npc = fact.CreateNpc(PILOT);
}
void Bridge::GetInfo() {
  std::cout << "> Currently in the " << name << std::endl;
  std::cout << "> You notice that there is one way out of this room:"
      << std::endl << "> left" << std::endl
      << "> you also notice the Pilot is here" << std::endl
      << "> and may want something " << std::endl;
}
void Bridge::UpWall(PlayerData *player) {
  std::cout << "> You decide to Stand on a near by table" << std::endl
      << "> it ends up collapsing taking you with it," << std::endl;
  player->UpdateHealth(-10);
}
void Bridge::DownWall(PlayerData *player) {
  std::cout << "> You decide to walk, but end up slipping" << std::endl
      << "> on a puddle hitting your head " << std::endl;
  player->UpdateHealth(-10);
}
void Bridge::RightWall(PlayerData *player) {
  std::cout << "> You decide to walk right, without paying attention, "
      << std::endl << "> and end up bumping " << std::endl
      << "> the corner of a desk" << std::endl;
  player->UpdateHealth(-10);
}
void Bridge::LeftWall(PlayerData *player) {
  std::cout << "> You move left" << std::endl;
  player->GetCords()->UpdateX(4);
}
/*
 * CrewQuaters implementation
 */
CrewQuaters::CrewQuaters() {
  name = "CrewQuaters";
  inventory = new Inventory();
  SetContents();
}
CrewQuaters::~CrewQuaters() {
  delete inventory;
  delete npc;
}
void CrewQuaters::SetContents() {
  NpcFact fact;
  npc = fact.CreateNpc(FLIGHTATTENDENT);
  inventory->AddItem(PATCH, "patch");
}
void CrewQuaters::GetInfo() {
  std::cout << "> Currently in the " << name << std::endl;
  std::cout << "> You notice that there is one way out of this room:"
      << std::endl << "> left" << std::endl
      << "> you also notice the Flight Attendent is here" << std::endl
      << "> and may want something " << std::endl;
}
void CrewQuaters::UpWall(PlayerData *player) {
  std::cout << "> You decide to start jumping on the bed " << std::endl
      << "> you end up falling an bumping your head," << std::endl;
  player->UpdateHealth(-10);
}
void CrewQuaters::DownWall(PlayerData *player) {
  std::cout << "> You decide to walk, but end up slipping" << std::endl
      << "> on a puddle hitting your head " << std::endl;
  player->UpdateHealth(-10);
}
void CrewQuaters::RightWall(PlayerData *player) {
  std::cout << "> You decide to walk right, without paying attention, "
      << std::endl << "> and end up bumping " << std::endl
      << "> a pot of boiling soup, burning you" << std::endl;
  player->UpdateHealth(-10);
}
void CrewQuaters::LeftWall(PlayerData *player) {
  std::cout << "> You move left" << std::endl;
  player->GetCords()->UpdateX(1);
}
/*
 * Bathroom implementation
 */
Bathroom::Bathroom() {
  name = "Bathroom";
  inventory = new Inventory();
  SetContents();
}
Bathroom::~Bathroom() {
  delete inventory;
  delete npc;
}
void Bathroom::SetContents() {
  NpcFact fact;
  npc = fact.CreateNpc(MEDIC);
  inventory->AddItem(MEDKIT, "medkit");
}
void Bathroom::GetInfo() {
  std::cout << "> Currently in the " << name << std::endl;
  std::cout << "> You notice that there is one way out of this room:"
      << std::endl << "> right" << std::endl
      << "> you also notice the Medic is here, and is able to heal you"
      << std::endl << "> but sadly the ship is low on medical supply"
      << std::endl;
}
void Bathroom::UpWall(PlayerData *player) {
  std::cout << "> You decide to start jumping on the bed " << std::endl
      << "> you end up falling an bumping your head," << std::endl;
  player->UpdateHealth(-10);
}
void Bathroom::DownWall(PlayerData *player) {
  std::cout << "> You decide to walk, but end up slipping" << std::endl
      << "> on a puddle hitting your head " << std::endl;
  player->UpdateHealth(-10);
}
void Bathroom::RightWall(PlayerData *player) {
  std::cout << "> You move right" << std::endl;
  player->GetCords()->UpdateX(1);
}
void Bathroom::LeftWall(PlayerData *player) {
  std::cout
      << "> you walk left, and crash into a IV drip, tumbling to the ground "
      << std::endl << "> and injuring yourself" << std::endl;
  player->UpdateHealth(-10);
}
/*
 * FoodStorage implementation
 */
FoodStorage::FoodStorage() {
  name = "FoodStorage";
  inventory = new Inventory();
  SetContents();
}
FoodStorage::~FoodStorage() {
  delete inventory;
  delete npc;
}
void FoodStorage::SetContents() {
  inventory->AddItem(TOOLS, "tools");
  inventory->AddItem(AIRTANK, "airtank");
}
void FoodStorage::GetInfo() {
  std::cout << "> Currently in the" << name << std::endl;
  std::cout << "> You notice that there is way out of this room:" << std::endl
      << "> left" << std::endl
      << "> you can see lots of items which may be of interest" << std::endl;
}
void FoodStorage::UpWall(PlayerData *player) {
  std::cout << "> You decide to climb on some of the shelves " << std::endl
      << "> but sadly the one you choose is needing maintenance and ends"
      << std::endl << "crashing to the ground taking you with it" << std::endl;
  player->UpdateHealth(-10);
}
void FoodStorage::DownWall(PlayerData *player) {
  std::cout << "> You decide to walk, but end up slipping" << std::endl
      << "> on a puddle hitting your head on a crate " << std::endl;
  player->UpdateHealth(-10);
}
void FoodStorage::RightWall(PlayerData *player) {
  std::cout << "> You move right, but you hit your foot on "
  << " a crate and stub your toes." << std::endl;
  player->UpdateHealth(-10);
}
void FoodStorage::LeftWall(PlayerData *player) {
  std::cout
      << "> You walk left" << std::endl;
  player->GetCords()->UpdateX(7);
}
/*
 * Kitchen implementation
 */
Kitchen::Kitchen() {
  name = "Kitchen";
  inventory = new Inventory();
  SetContents();
}
Kitchen::~Kitchen() {
  delete inventory;
  delete npc;
}
void Kitchen::SetContents() {
  inventory->AddItem(TOOLS, "tools");
  inventory->AddItem(AIRTANK, "airtank");
}
void Kitchen::GetInfo() {
  std::cout << "> Currently in the" << name << std::endl;
  std::cout << "> You notice that there is way out of this room:" << std::endl
      << "> left" << std::endl
      << "> you can see lots of items which may be of interest" << std::endl;
}
void Kitchen::UpWall(PlayerData *player) {
  std::cout << "> You decide to climb on some of the shelves " << std::endl
      << "> but sadly the one you choose is needing maintenance and ends"
      << std::endl << "crashing to the ground taking you with it" << std::endl;
  player->UpdateHealth(-10);
}
void Kitchen::DownWall(PlayerData *player) {
  std::cout << "> You decide to walk, but end up slipping" << std::endl
      << "> on a puddle hitting your head on a crate " << std::endl;
  player->UpdateHealth(-10);
}
void Kitchen::RightWall(PlayerData *player) {
  std::cout << "> You move right, but you hit your foot on "
  << " a crate and stub your toes." << std::endl;
  player->UpdateHealth(-10);
}
void Kitchen::LeftWall(PlayerData *player) {
  std::cout
      << "> You walk left" << std::endl;
  player->GetCords()->UpdateX(7);
}
