#include "GlobalVaribles.h"
#include "PlayerData.h"
#include "GameMap.h"
#include "Exceptions.h"
#include "gtest/gtest.h"
/*
 *  Testing Room's Inventory
 */
TEST(TestRoomInventory, TestRoom0Items) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(0);
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItemName("Medkit"), "");

  delete player;
  delete map;
}
/*
 *  Testing Room's Inventory
 */
TEST(TestRoomInventory, TestRoom1Items) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(1);
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItemName("Tools"), "Tools");

  delete player;
  delete map;
}
/*
 *  Testing Room's Inventory
 */
TEST(TestRoomInventory, TestRoom2Items) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(2);
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItemName("Patch"), "");

  delete player;
  delete map;
}
/*
 *  Testing Room's Inventory
 */
TEST(TestRoomInventory, TestRoom3Items) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(3);
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItemName("Patch"), "");
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItem("Patch"), nullptr);

  delete player;
  delete map;
}
/*
 *  Testing Room's Inventory
 */
TEST(TestRoomInventory, TestRoom4Items) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(4);
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItemName("Airtank"), "Airtank");

  delete player;
  delete map;
}
/*
 *  Testing Room's Inventory
 */
TEST(TestRoomInventory, TestRoom5Items) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(5);
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItemName("Patch"), "");
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItem("Patch"), nullptr);
  EXPECT_EQ(map->GetRoom(player->GetCords())
      ->GetReqInventory(), nullptr);

  delete player;
  delete map;
}
/*
 *  Testing Room's Inventory
 */
TEST(TestRoomInventory, TestRoom6Items) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(5);
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItemName("Patch"), "");
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItem("Patch"), nullptr);
  EXPECT_EQ(map->GetRoom(player->GetCords())
      ->GetReqInventory(), nullptr);

  delete player;
  delete map;
}
/*
 *  Testing Room's Inventory
 */
TEST(TestRoomInventory, TestRoom7Items) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(8);
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItemName("Airtank"), "");
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItemName("Tools"), "");

  delete player;
  delete map;
}
/*
 *  Testing Room's Inventory
 */
TEST(TestRoomInventory, TestRoom8Items) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(8);
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItemName("Airtank"), "");
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItemName("Tools"), "");

  delete player;
  delete map;
}
/*
 *  Testing Room's Inventory
 */
TEST(TestRoomInventory, TestRoom9Items) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(9);
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItemName("CockPitParts"), "");

  delete player;
  delete map;
}
/*
 *  Testing Room's Inventory
 */
TEST(TestRoomInventory, TestRoom10Items) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(10);
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItemName("CockPitParts"), "");
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItem("CockPitParts"), nullptr);
  delete player;
  delete map;
}
/*
 *  Testing Room's Inventory
 */
TEST(TestRoomInventory, TestRoom11Items) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(11);
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItemName("Tools"), "");
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItemName("PlaneParts"), "");
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItemName("EngineParts"), "");
  EXPECT_EQ(map->GetRoomContents(player->GetCords())
      ->GetItemName("Medal"), "");
  delete player;
  delete map;
}
