#include "PlayerData.h"
#include "GameMap.h"
#include "ItemStorage.h"
#include "ItemCommands.h"
#include "RoomCommands.h"
#include "Exceptions.h"
#include "gtest/gtest.h"
/*
 * Testing AirFillng in Airtank
 */
TEST(TestItemInteract, testFillAir) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  PickUp* pickUp = new PickUp(map, player);
  FillAir* fillAir = new FillAir(player);

  player->GetCords()->OverRideCord(8);
  EXPECT_EQ(player->GetOxygen(), 100);
  player->UpdateOxygen(-10);
  EXPECT_EQ(player->GetOxygen(), 90);
  fillAir->Run();
  EXPECT_EQ(player->GetOxygen(), 90);
  EXPECT_EQ(player->GetInventory()->GetItem("Airtank"), nullptr);
  pickUp->Run("Airtank");
  EXPECT_EQ(player->GetOxygen(), 90);
  fillAir->Run(); // filling air
  EXPECT_EQ(player->GetOxygen(), 90);
  delete map;
  delete player;
  delete pickUp;
  delete fillAir;
}
/*
 * Testing Engine Fixing Using EngineParts
 */
TEST(TestItemInteract, testEngineFix) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  PickUp* pickUp = new PickUp(map, player);
  FixRoom* fix = new FixRoom(map, player);
  player->GetCords()->OverRideCord(6);
  fix->Run();
  EXPECT_FALSE(map->GetRoom(player->GetCords())->RoomFixed());

  player->GetCords()->OverRideCord(11);
  EXPECT_EQ(player->GetInventory()->GetItem("Tools"), nullptr);
  EXPECT_EQ(player->GetInventory()->GetItem("PlaneParts"), nullptr);
  EXPECT_EQ(player->GetInventory()->GetItem("EngineParts"), nullptr);
  pickUp->Run("Tools");
  pickUp->Run("PlaneParts");
  pickUp->Run("EngineParts");
  EXPECT_FALSE(map->GetRoom(player->GetCords())->RoomFixed());
  EXPECT_EQ(player->GetInventory()->GetItem("Patch"), nullptr);
  EXPECT_EQ(player->GetInventory()->GetItem("Tools"), nullptr);
  delete map;
  delete player;
  delete fix;
  delete pickUp;
}
/*
 * Testing Fixing Airlock using Airtank
 */
TEST(TestItemInteract, testFixAirLock) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  PickUp* pickUp = new PickUp(map, player);
  FixRoom* fix = new FixRoom(map, player);
  player->GetCords()->OverRideCord(7);
  fix->Run();
  EXPECT_FALSE(map->GetRoom(player->GetCords())->RoomFixed());

  player->GetCords()->OverRideCord(2);
  EXPECT_EQ(player->GetInventory()->GetItem("Patch"), nullptr);
  EXPECT_EQ(player->GetInventory()->GetItem("Tools"), nullptr);
  pickUp->Run("Patch");
  player->GetCords()->OverRideCord(8);
  EXPECT_EQ(map->GetRoom(player->GetCords())->GetType(), "FoodStorage");
  pickUp->Run("Tools");
  player->GetCords()->OverRideCord(7);
  fix->Run();
  EXPECT_FALSE(map->GetRoom(player->GetCords())->RoomFixed());
  EXPECT_EQ(player->GetInventory()->GetItem("Patch"), nullptr);
  EXPECT_EQ(player->GetInventory()->GetItem("Tools"), nullptr);
  delete map;
  delete player;
  delete pickUp;
  delete fix;
}
/*
 * Testing Dropping
 */
TEST(TestItemInteract, testDroping) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  PickUp* pickUp = new PickUp(map, player);
  FixRoom* fix = new FixRoom(map, player);
  player->GetCords()->OverRideCord(7);
  fix->Run();
  EXPECT_FALSE(map->GetRoom(player->GetCords())->RoomFixed());

  player->GetCords()->OverRideCord(2);
  EXPECT_EQ(player->GetInventory()->GetItem("Patch"), nullptr);
  EXPECT_EQ(player->GetInventory()->GetItem("Tools"), nullptr);
  pickUp->Run("Patch");
  player->GetCords()->OverRideCord(8);
  EXPECT_EQ(map->GetRoom(player->GetCords())->GetType(), "FoodStorage");
  pickUp->Run("Tools");
  player->GetCords()->OverRideCord(7);
  fix->Run();
  EXPECT_FALSE(map->GetRoom(player->GetCords())->RoomFixed());
  EXPECT_EQ(player->GetInventory()->GetItem("Patch"), nullptr);
  EXPECT_EQ(player->GetInventory()->GetItem("Tools"), nullptr);
  delete map;
  delete player;
  delete pickUp;
  delete fix;
}
