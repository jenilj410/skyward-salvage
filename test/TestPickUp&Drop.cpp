#include "PlayerData.h"
#include "GameMap.h"
#include "ItemStorage.h"
#include "ItemCommands.h"
#include "RoomCommands.h"
#include "Exceptions.h"
#include "gtest/gtest.h"

/*
 *  Testing Pickup Items in Room 0
 */
TEST(TestRoomPickUp, TestRoom0PickUp) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(0);
  Up* up = new Up(map, player);
  up->Run();
  EXPECT_EQ(player->GetCords()->GetX(), 0);
  EXPECT_EQ(player->GetHealth(), 90);
  delete up;
  delete player;
  delete map;
}
/*
 *  Testing Pickup Items in Room 1
 */
TEST(TestRoomPickUp, TestRoom1PickUp) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(0);
  Left* left = new Left(map, player);
  left->Run();
  EXPECT_EQ(player->GetCords()->GetX(), 7);
  EXPECT_EQ(player->GetHealth(), 100);
  delete left;
  delete player;
  delete map;
}
/*
 *  Testing Pickup Items in Room 2
 */
TEST(TestRoomPickUp, TestRoom2PickUp) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(0);
  Right* right = new Right(map, player);
  right->Run();
  EXPECT_EQ(player->GetCords()->GetX(), 0);
  EXPECT_EQ(player->GetHealth(), 90);
  delete right;
  delete player;
  delete map;
}
/*
 *  Testing Pickup Items in Room 3
 */
TEST(TestRoomPickUp, TestRoom3PickUp) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(0);
  Down* down = new Down(map, player);
  down->Run();
  EXPECT_EQ(player->GetCords()->GetX(), 0);
  EXPECT_EQ(player->GetHealth(), 90);
  delete down;
  delete player;
  delete map;
}
/*
 *  Testing Pickup Items in Room 4
 */
TEST(TestRoomPickUp, TestRoom4PickUp) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(0);
  Up* up = new Up(map, player);
  up->Run();
  EXPECT_EQ(player->GetCords()->GetX(), 0);
  EXPECT_EQ(player->GetHealth(), 90);
  delete up;
  delete player;
  delete map;
}
/*
 *  Testing Pickup Items in Room 5
 */
TEST(TestRoomPickUp, TestRoom5PickUp) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(0);
  Left* left = new Left(map, player);
  left->Run();
  EXPECT_EQ(player->GetCords()->GetX(), 7);
  EXPECT_EQ(player->GetHealth(), 100);
  delete left;
  delete player;
  delete map;
}
/*
 *  Testing Pickup Items in Room 6
 */
TEST(TestRoomPickUp, TestRoom6PickUp) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(0);
  Right* right = new Right(map, player);
  right->Run();
  EXPECT_EQ(player->GetCords()->GetX(), 0);
  EXPECT_EQ(player->GetHealth(), 90);
  delete right;
  delete player;
  delete map;
}
/*
 *  Testing Pickup Items in Room 7
 */
TEST(TestRoomPickUp, TestRoom7PickUp) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(0);
  Down* down = new Down(map, player);
  down->Run();
  EXPECT_EQ(player->GetCords()->GetX(), 0);
  EXPECT_EQ(player->GetHealth(), 90);
  delete down;
  delete player;
  delete map;
}
/*
 *  Testing Pickup Items in Room 8
 */
TEST(TestRoomPickUp, TestRoom8PickUp) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(0);
  Up* up = new Up(map, player);
  up->Run();
  EXPECT_EQ(player->GetCords()->GetX(), 0);
  EXPECT_EQ(player->GetHealth(), 90);
  delete up;
  delete player;
  delete map;
}
/*
 *  Testing Pickup Items in Room 9
 */
TEST(TestRoomPickUp, TestRoom9PickUp) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(0);
  Left* left = new Left(map, player);
  left->Run();
  EXPECT_EQ(player->GetCords()->GetX(), 7);
  EXPECT_EQ(player->GetHealth(), 100);
  delete left;
  delete player;
  delete map;
}
/*
 *  Testing Pickup Items in Room 10
 */
TEST(TestRoomPickUp, TestRoom14PickUp) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(0);
  Right* right = new Right(map, player);
  right->Run();
  EXPECT_EQ(player->GetCords()->GetX(), 0);
  EXPECT_EQ(player->GetHealth(), 90);
  delete right;
  delete player;
  delete map;
}
/*
 *  Testing Pickup Items in Room 11
 */
TEST(TestRoomPickUp, TestRoom10PickUp) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(0);
  Down* down = new Down(map, player);
  down->Run();
  EXPECT_EQ(player->GetCords()->GetX(), 0);
  EXPECT_EQ(player->GetHealth(), 90);
  delete down;
  delete player;
  delete map;
}
/*
 *  Testing Pickup Items in Room 12
 */
TEST(TestRoomPickUp, TestRoom11PickUp) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(0);
  Up* up = new Up(map, player);
  up->Run();
  EXPECT_EQ(player->GetCords()->GetX(), 0);
  EXPECT_EQ(player->GetHealth(), 90);
  delete up;
  delete player;
  delete map;
}
/*
 *  Testing Pickup Items in Room 13
 */
TEST(TestRoomPickUp, TestRoom12PickUp) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(0);
  Left* left = new Left(map, player);
  left->Run();
  EXPECT_EQ(player->GetCords()->GetX(), 7);
  EXPECT_EQ(player->GetHealth(), 100);
  delete left;
  delete player;
  delete map;
}
