#include "PlayerData.h"
#include "GameMap.h"
#include "ItemStorage.h"
#include "ItemCommands.h"
#include "RoomCommands.h"
#include "NpcCommands.h"
#include "Exceptions.h"
#include "gtest/gtest.h"
/*
 *  Testing NPC Healing
 */
TEST(TestNPCInteract, TestHealing) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(0);
  PickUp* pickUp = new PickUp(map, player);
  FinishQuest* finishQuest = new FinishQuest(map, player);

  Heal* heal = new Heal(map, player);
  heal->Run();

  EXPECT_EQ(player->GetInventory()->GetItem("Medkit"), nullptr);
  pickUp->Run("Medkit");
  heal->Run();
  EXPECT_EQ(player->GetHealth(), 100);
  player->UpdateHealth(-20);
  EXPECT_EQ(player->GetHealth(), 80);
  heal->Run();
  finishQuest->Run();
  EXPECT_EQ(player->GetHealth(), 80);
  delete map;
  delete player;
  delete pickUp;
  delete heal;
  delete finishQuest;
}
/*
 *  Testing Pilot's Quest
 */
TEST(TestNPCInteract, TestPilotQuest) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(11);
  PickUp* pickUp = new PickUp(map, player);
  FinishQuest* finishQuest = new FinishQuest(map, player);

  player->GetCords()->OverRideCord(11);
  EXPECT_EQ(player->GetInventory()->GetItem("Medal"), nullptr);
  pickUp->Run("Medal");
  player->GetCords()->OverRideCord(BRIDGE_CORDS);
  finishQuest->Run();
  EXPECT_EQ(player->GetInventory()->GetItem("Medal"), nullptr);
  delete map;
  delete player;
  delete pickUp;
  delete finishQuest;
}
/*
 *  Testing FlightAttendent's Quest
 */
TEST(TestNPCInteract, TestFlightAttendentQuest) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  player->GetCords()->OverRideCord(9);
  PickUp* pickUp = new PickUp(map, player);
  Heal* heal = new Heal(map, player);
  FinishQuest* finishQuest = new FinishQuest(map, player);

  EXPECT_EQ(player->GetInventory()->GetItem("CockPitParts"), nullptr);
  pickUp->Run("CockPitParts");
  player->GetCords()->OverRideCord(CREWQUATERS_CORDS);
  finishQuest->Run();
  heal->Run();
  EXPECT_EQ(player->GetInventory()->GetItem("CockPitParts"), nullptr);
  delete map;
  delete player;
  delete pickUp;
  delete finishQuest;
  delete heal;
}
/*
 *  Testing NPC's Talk
 */
TEST(TestNPCInteract, TestTalking) {
  GameMap *map = new GameMap();
  PlayerData* player = new PlayerData();
  Talk* talk = new Talk(map, player->GetCords());
  player->GetCords()->OverRideCord(CREWQUATERS_CORDS);
  talk->Run();
  player->GetCords()->OverRideCord(0);
  talk->Run();
  player->GetCords()->OverRideCord(BRIDGE_CORDS);
  talk->Run();
  delete map;
  delete player;
  delete talk;
}
