#include "GameMap.h"
#include <NpcFact.h>
#include "Inventory.h"
#include "ItemStorage.h"
#include "Exceptions.h"
#include "gtest/gtest.h"

/*
 * Testing Item Generation
 */
TEST(TestMapGen, TestItemGen) {
  ItemStorage itemfact;

  Item* tools = itemfact.CreateItem(TOOLS);
  EXPECT_EQ(tools->GetName(), "Tools");
  tools->GetDescription();
  delete tools;
  Item* CockpitParts = itemfact.CreateItem(COCKPITPARTS);
  EXPECT_EQ(CockpitParts->GetName(), "CockPitParts");
  CockpitParts->GetDescription();
  delete CockpitParts;
  Item* PlanePart = itemfact.CreateItem(PLANEPARTS);
  EXPECT_EQ(PlanePart->GetName(), "PlaneParts");
  PlanePart->GetDescription();
  delete PlanePart;
  Item* EnginePart = itemfact.CreateItem(ENGINEPARTS);
  EXPECT_EQ(EnginePart->GetName(), "EngineParts");
  EnginePart->GetDescription();
  delete EnginePart;
  Item* patch = itemfact.CreateItem(PATCH);
  EXPECT_EQ(patch->GetName(), "Patch");
  patch->GetDescription();
  delete patch;
  Item* medkit = itemfact.CreateItem(MEDKIT);
  EXPECT_EQ(medkit->GetName(), "Medkit");
  medkit->GetDescription();
  delete medkit;
  Item* medal = itemfact.CreateItem(MEDAL);
  EXPECT_EQ(medal->GetName(), "Medal");
  medal->GetDescription();
  delete medal;
  Item* airtank = itemfact.CreateItem(AIRTANK);
  EXPECT_EQ(airtank->GetName(), "Airtank");
  airtank->GetDescription();
  delete airtank;
}
/*
 * Testing Romm's Facts
 */
TEST(TestMapGen, TestRoomFact) {
  MapGen* gen = new MapGen();
  Environment* env = gen->CreateMapLocation(BRIDGE);
  EXPECT_EQ(env->GetType(), "Bridge");
  env->GetInfo();
  delete env;
  env = gen->CreateMapLocation(COCKPIT);
  EXPECT_EQ(env->GetType(), "CockPit");
  env->GetInfo();
  delete env;
  env = gen->CreateMapLocation(ENGINEROOM);
  EXPECT_EQ(env->GetType(), "EngineRoom2");
  env->GetInfo();
  delete env;
  env = gen->CreateMapLocation(FOODSTORAGE);
  EXPECT_EQ(env->GetType(), "FoodStorage");
  env->GetInfo();
  delete env;
  env = gen->CreateMapLocation(ELEVATOR);
  EXPECT_EQ(env->GetType(), "ElevatorRoom");
  env->GetInfo();
  delete env;
  env = gen->CreateMapLocation(BATHROOM);
  EXPECT_EQ(env->GetType(), "Bathroom");
  env->GetInfo();
  delete env;
  env = gen->CreateMapLocation(CREWQUATERS);
  EXPECT_EQ(env->GetType(), "CrewQuaters");
  delete env;
  env = gen->CreateMapLocation(KITCHEN);
  EXPECT_EQ(env->GetType(), "Kitchen");
  env->GetInfo();
  delete env;
  env = gen->CreateMapLocation(TOPAIRLOCK);
  EXPECT_EQ(env->GetType(), "TopAirlock");
  env->GetInfo();
  delete env;
  env = gen->CreateMapLocation(BAGGAGESTORAGE);
  EXPECT_EQ(env->GetType(), "BaggageStorage1");
  env->GetInfo();
  delete env;
  env = gen->CreateMapLocation(WHEELWELL1);
  EXPECT_EQ(env->GetType(), "WheelWell1");
  env->GetInfo();
  delete env;
  env = gen->CreateMapLocation(WHEELWELL2);
  EXPECT_EQ(env->GetType(), "WheelWell2");
  env->GetInfo();
  delete env;
  env = gen->CreateMapLocation(TOPAIRLOCK);
  EXPECT_EQ(env->GetType(), "TopAirlock");
  env->GetInfo();
  delete env;
  delete gen;
}
/*
 * Testing Map Generation
 */
TEST(TestMapGen, TestMapGenration) {
  GameMap* map = new GameMap();
  //Kitchen
  EXPECT_EQ(map->GetRoom(KITCHEN_CORDS)->GetType(), "Kitchen");
  //TOPAIRLOCK
  EXPECT_EQ(map->GetRoom(TOPAIRLOCK_CORDS)->GetType(), "TopAirlock");
  //CrewQuaters
  EXPECT_EQ(map->GetRoom(COCKPIT)->GetType(), "CrewQuaters");
  //ElevatorRoom
  EXPECT_EQ(map->GetRoom(ELEVATOR_CORDS)->GetType(), "ElevatorRoom");
  //BRIDGE
  EXPECT_EQ(map->GetRoom(BRIDGE_CORDS)->GetType(), "Bridge");
  //EngineRoom2
  EXPECT_EQ(map->GetRoom(ENGINEROOM_CORDS)->GetType(), "EngineRoom2");
  //TopAirlock
  EXPECT_EQ(map->GetRoom(TOPAIRLOCK_CORDS)->GetType(), "TopAirlock");
  //FoodStorage
  EXPECT_EQ(map->GetRoom(FOODSTORAGE_CORDS)->GetType(), "FoodStorage");
  //WheelWell1
  EXPECT_EQ(map->GetRoom(WHEELWELL1_CORDS)->GetType(), "WheelWell1");
  //WheelWell2
  EXPECT_EQ(map->GetRoom(WHEELWELL2_CORDS)->GetType(), "WheelWell2");
  //BaggageStorage1
  EXPECT_EQ(map->GetRoom(BAGGAGESTORAGE_CORDS)->GetType(), "BaggageStorage1");
  delete map;
}
/*
 * Testing NPC Generation
 */
TEST(TestMapGen, TestNPCGenration) {
  GameMap* map = new GameMap();
  EXPECT_EQ(map->GetRoom(BRIDGE_CORDS)->GetNPC()->GetName(), "Pilot");
  EXPECT_EQ(map->GetRoom(CREWQUATERS_CORDS)->GetNPC()
  ->GetName(), "FlightAttendent");
  EXPECT_EQ(map->GetRoom(7)->GetNPC(), nullptr);
  delete map;
}
/*
 * Testing NPC's Item Generation
 */
TEST(TestMapGen, TestNPCItemGenration) {
  GameMap* map = new GameMap();
  EXPECT_EQ(map->GetRoom(BRIDGE_CORDS)->GetNPC()
      ->GetQuest()->GetName(), "Medal");
  EXPECT_EQ(map->GetRoom(CREWQUATERS_CORDS)->GetNPC()
      ->GetQuest()->GetName(), "CockPitParts");
  delete map;
}
/*
 * Testing NPC's Null Generation
 */
TEST(TestMapGen, TestNPCNullGenration) {
  NpcFact* fact = new NpcFact();
  EXPECT_EQ(fact->CreateNpc(NONE), nullptr);
  delete fact;
}
/*
 * Testing Rooms's Null Generation
 */
TEST(TestMapGen, TestRoomNullGenration) {
  MapGen* fact = new MapGen();
  EXPECT_EQ(fact->CreateMapLocation(AIR), nullptr);
  delete fact;
}
