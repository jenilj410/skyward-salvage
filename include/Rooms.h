/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-11-27
 */

#ifndef ROOMS_H_INCLUDED
#define ROOMS_H_INCLUDED

#include "Environment.h"
#include <iostream>
#include <string>

/**
 * @class Kitchen Rooms.h  "Rooms.h"
 * @brief concrete class to define a Kitchen room
 */
class Kitchen : public Environment {
 public:
  /**
   * @brief default constructor
   */
  Kitchen();
  /**
   * @brief default destructor
   */
  virtual ~Kitchen();
  /**
   * @brief outputs the rooms info
   * @return a string  with room info
   */
  void GetInfo();
  /**
   * @brief makes up movement not possible and injure player
   * @parm [in] playerData
   */
  void UpWall(PlayerData *player);
  /**
   * @brief makes down movement not possible and injure player
   * @parm [in] playerData
   */
  void DownWall(PlayerData *player);
  /**
   * @brief defines right direction
   * @parm [in] playerData
   */
  void RightWall(PlayerData *player);
  /**
   * @brief defines left direction
   * @parm [in] playerData
   */
  void LeftWall(PlayerData *player);

 private:
  /**
   * @brief sets contents of locations inventory
   */
  void SetContents();
};

/**
 * @class Bathroom Rooms.h  "Rooms.h"
 * @brief concrete class to define a Bathroom room
 */
class Bathroom : public Environment {
 public:
  /**
   * @brief default constructor
   */
  Bathroom();
  /**
   * @brief default destructor
   */
  virtual ~Bathroom();
  /**
   * @brief outputs the rooms info
   * @return a string  with room info
   */
  void GetInfo();
  /**
   * @brief makes up movement not possible and injure player
   * @parm [in] playerData
   */
  void UpWall(PlayerData *player);
  /**
   * @brief makes down movement not possible and injure player
   * @parm [in] playerData
   */
  void DownWall(PlayerData *player);
  /**
   * @brief defines right direction
   * @parm [in] playerData
   */
  void RightWall(PlayerData *player);
  /**
   * @brief defines left direction
   * @parm [in] playerData
   */
  void LeftWall(PlayerData *player);

 private:
  /**
   * @brief sets contents of locations inventory
   */
  void SetContents();
};

/**
 * @class CrewQuaters Rooms.h  "Rooms.h"
 * @brief concrete class to define a CrewQuaters room
 */
class CrewQuaters : public Environment {
 public:
  /**
   * @brief default constructor
   */
  CrewQuaters();
  /**
   * @brief default destructor
   */
  virtual ~CrewQuaters();
  /**
   * @brief outputs the rooms info
   * @return a string  with room info
   */
  void GetInfo();
  /**
   * @brief makes up movement not possible and injure player
   * @parm [in] playerData
   */
  void UpWall(PlayerData *player);
  /**
   * @brief makes down movement not possible and injure player
   * @parm [in] playerData
   */
  void DownWall(PlayerData *player);
  /**
   * @brief defines right direction
   * @parm [in] playerData
   */
  void RightWall(PlayerData *player);
  /**
   * @brief defines left direction
   * @parm [in] playerData
   */
  void LeftWall(PlayerData *player);

 private:
  /**
   * @brief sets contents of locations inventory
   */
  void SetContents();
};

/**
 * @class Bridge Rooms.h  "Rooms.h"
 * @brief concrete class to define a Bridge room
 */
class Bridge : public Environment {
 public:
  /**
   * @brief default constructor
   */
  Bridge();
  /**
   * @brief default destructor
   */
  virtual ~Bridge();
  /**
   * @brief outputs the rooms info
   * @return a string  with room info
   */
  void GetInfo();
  /**
   * @brief makes up movement not possible and injure player
   * @parm [in] playerData
   */
  void UpWall(PlayerData *player);
  /**
   * @brief makes down movement not possible and injure player
   * @parm [in] playerData
   */
  void DownWall(PlayerData *player);
  /**
   * @brief defines right direction
   * @parm [in] playerData
   */
  void RightWall(PlayerData *player);
  /**
   * @brief defines left direction
   * @parm [in] playerData
   */
  void LeftWall(PlayerData *player);

 private:
  /**
   * @brief sets contents of locations inventory
   */
  void SetContents();
};

/**
 * @class FoodStorage Rooms.h  "Rooms.h"
 * @brief concrete class to define a FoodStorage room
 */
class FoodStorage : public Environment {
 public:
  /**
   * @brief default constructor
   */
  FoodStorage();
  /**
   * @brief default destructor
   */
  virtual ~FoodStorage();
  /**
   * @brief outputs the rooms info
   * @return a string  with room info
   */
  void GetInfo();
  /**
   * @brief makes up movement not possible and injure player
   * @parm [in] playerData
   */
  void UpWall(PlayerData *player);
  /**
   * @brief makes down movement not possible and injure player
   * @parm [in] playerData
   */
  void DownWall(PlayerData *player);
  /**
   * @brief defines right direction
   * @parm [in] playerData
   */
  void RightWall(PlayerData *player);
  /**
   * @brief defines left direction
   * @parm [in] playerData
   */
  void LeftWall(PlayerData *player);

 private:
  /**
   * @brief sets contents of locations inventory
   */
  void SetContents();
};
#endif //ROOMS_H_INCLUDED
