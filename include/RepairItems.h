/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-11-27
 */
#ifndef REPAIRPARTS_H_INCLUDED
#define REPAIRPARTS_H_INCLUDED
#include "Item.h"
#include <iostream>
#include <string>
/**
 * @class EngineParts EngineParts.h  "EngineParts.h"
 * @brief concrete class to define a EngineParts Item
 */
class EngineParts : public Item {
 public:
  /**
   * @brief default constructor
   */
  EngineParts();
  /**
   * @brief default destructor
   */
  ~EngineParts();
  void GetDescription();
};
/**
 * @class Patch Patch.h  "Patch.h"
 * @brief concrete class to define a Patch Item
 */
class Patch : public Item {
 public:
  /**
   * @brief default constructor
   */
  Patch();
  /**
   * @brief default destructor
   */
  ~Patch();
  void GetDescription();
};
class PlaneParts : public Item {
 public:
  /**
   * @brief Default Constructor
   */
  PlaneParts();
  /**
   * @brief Default Destructor
   */
  virtual ~PlaneParts();
  /**
   * @brief Outputs description of item
   * @return string of words
   */
  void GetDescription();
};
/**
 * @class Tools Tools.h  "Tools.h"
 * @brief concrete class to define a Patch Item
 */
class Tools : public Item {
 public:
  /**
   * @brief Default Constructor
   */
  Tools();
  /**
   * @brief Default Destructor
   */
  virtual ~Tools();
  /**
   * @brief Outputs description of item
   * @return string of words
   */
  void GetDescription();
};
#endif //REPAIRPARTS_H_INCLUDED
