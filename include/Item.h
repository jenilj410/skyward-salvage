/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-11-27
 */
#ifndef ITEM_H_INCLUDED
#define ITEM_H_INCLUDED
#include <iostream>
#include <string>

class Item {
 public:
  /**
   * @brief Default Constructor
   */
  Item();
  /**
   * @brief Default Destructor
   */
  virtual ~Item();
  /**
   * @brief Outputs description of item
   * @return string of words
   */
  virtual void GetDescription() = 0;
  /**
   * @brief Gets item name
   * @return name of item
   */
  std::string GetName();

 protected:
  std::string name;
};
#endif //ITEM_H_INCLUDED
