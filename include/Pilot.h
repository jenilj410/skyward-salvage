/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-11-27
 */

#ifndef PILOT_H_INCLUDED
#define PILOT_H_INCLUDED

#include <Npc.h>
#include <string>

class Pilot : public Npc {
 public:
  /**
   * @brief default constructor
   */
  Pilot();
  /**
   * @brief default Destructor
   */
  virtual ~Pilot();
  /**
   * @brief outputs the Npc dialog
   * @return Npc dialog as a string
   */
  void GetDialog();
  /**
   * @brief Checks status of NPC quest
   */
  void FinsihQuest(PlayerData* player);
};
#endif // PILOT_H_INCLUDED
