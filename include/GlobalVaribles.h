/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-11-27
 */
#ifndef GLOBALVARIBLES_H_INCLUDED
#define GLOBALVARIBLES_H_INCLUDED

/**
 * @brief the maps width
 */
const int WIDTH = 4;
/**
 * @brief the maps Height
 */
const int HEIGHT = 4;

/**
 * @brief the spawn room varible
 */
const int KITCHEN_CORDS = 0;
const int TOPAIRLOCK_CORDS = 1;
const int CREWQUATERS_CORDS = 2;
const int COCKPIT_CORDS = 3;
const int ELEVATOR_CORDS = 4;
const int BRIDGE_CORDS = 5;
const int BATHROOM_CORDS = 6;
const int ENGINEROOM_CORDS = 7;
const int FOODSTORAGE_CORDS = 8;
const int WHEELWELL1_CORDS = 9;
const int BAGGAGESTORAGE_CORDS = 10;
const int WHEELWELL2_CORDS = 11;


/**
 * @brief Rooms in the game
 */
enum RoomType {
  AIR,
  BRIDGE,
  COCKPIT,
  ENGINEROOM,
  FOODSTORAGE,
  ELEVATOR,
  CREWQUATERS,
  KITCHEN,
  BATHROOM,
  BAGGAGESTORAGE,
  WHEELWELL1,
  WHEELWELL2,
  TOPAIRLOCK,
};
/**
 * @brief items in the game
 */
enum ItemType {
  EMPTY,
  AIRTANK,
  PLANEPARTS,
  ENGINEPARTS,
  PATCH,
  TOOLS,
  COCKPITPARTS,
  MEDKIT,
  MEDAL,
};
/**
 * @brief NPCS in the game
 */
enum NPCType {
  NONE,
  MEDIC,
  FLIGHTATTENDENT,
  PILOT,
};

#endif //GLOBALVARIBLES_H_INCLUDED
