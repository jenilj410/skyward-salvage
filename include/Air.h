/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-11-27
 */

#ifndef AIR_H_INCLUDED
#define AIR_H_INCLUDED

#include "Environment.h"
#include <iostream>
#include <string>
/**
 * @class BaggageStorage1 Air.h  "Air.h"
 * @brief concrete class to define a BaggageStorage1
 */
class BaggageStorage1 : public Environment {
 public:
  /**
   * @brief default constructor
   */
  BaggageStorage1();
  /**
   * @brief default destructor
   */
  virtual ~BaggageStorage1();
  /**
   * @brief outputs the rooms info
   * @return a string  with room info
   */
  void GetInfo();
  /**
   * @brief makes up movement not possible and injure player
   * @parm [in] playerData
   */
  void UpWall(PlayerData *player);
  /**
   * @brief makes down movement not possible and injure player
   * @parm [in] playerData
   */
  void DownWall(PlayerData *player);
  /**
   * @brief makes right movement not possible and injure player
   * @parm [in] playerData
   */
  void RightWall(PlayerData *player);
  /**
   * @brief makes left movement not possible and injure player
   * @parm [in] playerData
   */
  void LeftWall(PlayerData *player);

 private:
  /**
   * @brief sets contents of locations inventory
   */
  void SetContents();
};
/**
 * @class WheelWell1 Air.h  "Air.h"
 * @brief concrete class to define a WheelWell1
 */
class WheelWell1 : public Environment {
 public:
  /**
   * @brief default constructor
   */
  WheelWell1();
  /**
   * @brief default destructor
   */
  virtual ~WheelWell1();
  /**
   * @brief outputs the rooms info
   * @return a string  with room info
   */
  void GetInfo();
  /**
   * @brief makes up movement not possible and injure player
   * @parm [in] playerData
   */
  void UpWall(PlayerData *player);
  /**
   * @brief makes down movement not possible and injure player
   * @parm [in] playerData
   */
  void DownWall(PlayerData *player);
  /**
   * @brief Makes not possible
   * @parm [in] playerData
   */
  void RightWall(PlayerData *player);
  /**
   * @brief defines left direction
   * @parm [in] playerData
   */
  void LeftWall(PlayerData *player);

 private:
  /**
   * @brief sets contents of locations inventory
   */
  void SetContents();
};

/**
 * @class EngineRoom Air.h  "Air.h"
 * @brief concrete class to define a EngineRoom room
 */
class EngineRoom1 : public Environment {
 public:
  /**
   * @brief default constructor
   */
  EngineRoom1();
  /**
   * @brief default destructor
   */
  virtual ~EngineRoom1();
  /**
   * @brief outputs the rooms info
   * @return a string  with room info
   */
  void GetInfo();
  /**
   * @brief defines Up Movement
   * @parm [in] playerData
   */
  void UpWall(PlayerData *player);
  /**
   * @brief makes down movement not possible and injure player
   * @parm [in] playerData
   */
  void DownWall(PlayerData *player);
  /**
   * @brief defines right direction
   * @parm [in] playerData
   */
  void RightWall(PlayerData *player);
  /**
   * @brief @brief defines left direction
   * @parm [in] playerData
   */
  void LeftWall(PlayerData *player);

 private:
  /**
   * @brief sets contents of locations inventory
   */
  void SetContents();
};
#endif //AIR_H_INCLUDED
