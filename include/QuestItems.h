/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-11-27
 */
#ifndef QUESTIEMS_H_INCLUDED
#define QUESTIEMS_H_INCLUDED
#include "Item.h"
#include <iostream>
#include <string>
/**
 * @class CockpitParts CockpitParts.h  "CockpitParts.h"
 * @brief concrete class to define a CockpitParts Item
 */
class CockpitParts : public Item {
 public:
  /**
   * @brief default constructor
   */
  CockpitParts();
  /**
   * @brief default destructor
   */
  ~CockpitParts();
  void GetDescription();
};
/**
 * @class Medal Medal.h  "Medal.h"
 * @brief concrete class to define a Medal Item
 */
class Medal : public Item {
 public:
  /**
   * @brief default constructor
   */
  Medal();
  /**
   * @brief default destructor
   */
  ~Medal();
  void GetDescription();
};
#endif //QUESTIEMS_H_INCLUDED
