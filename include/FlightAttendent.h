/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-11-27
 */

#ifndef FLIGHTATTENDENT_H_INCLUDED
#define FLIGHTATTENDENT_H_INCLUDED
#include "Npc.h"

class FlightAttendent: public Npc {
 public:
  /**
   * @brief default constructor
   */
  FlightAttendent();
  /**
   * @brief default Destructor
   */
  virtual ~FlightAttendent();
  /**
   * @brief outputs the Npc dialog
   * @return Npc dialog as a string
   */
  void GetDialog();
  /**
   * @brief Checks status of NPC quest
   * @return bool value based on quest status
   */
  void FinsihQuest(PlayerData* player);
};
#endif // FLIGHTATTENDENT_H_INCLUDED
