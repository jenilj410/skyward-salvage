/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-11-27
 */
#ifndef CONSUMABLES_H_INCLUDED
#define CONSUMABLES_H_INCLUDED
#include "Item.h"
#include <iostream>
#include <string>
/**
 * @class AirTank AirTank.h  "AirTank.h"
 * @brief concrete class to define a AirTank Item
 */
class AirTank : public Item {
 public:
  /**
   * @brief default constructor
   */
  AirTank();
  /**
   * @brief default destructor
   */
  ~AirTank();
  void GetDescription();
};
/**
 * @class MedKit MedKit.h  "MedKit.h"
 * @brief concrete class to define a MedKit Item
 */
class MedKit : public Item {
 public:
  /**
   * @brief default constructor
   */
  MedKit();
  /**
   * @brief default destructor
   */
  ~MedKit();
  void GetDescription();
};
#endif // CONSUMABLES_H_INCLUDED
