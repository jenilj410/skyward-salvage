/**
 * @author Jenil Jani <jenil.jani@uleth.ca>
 * @date 2023-11-27
 */

#ifndef NPCFACT_H_INCLUDED
#define NPCFACT_H_INCLUDED

#include <GlobalVaribles.h>
#include <Npc.h>
#include <FlightAttendent.h>
#include <Pilot.h>
#include <Medic.h>
/**
 * @class NpcFact NpcFact.h  "NpcFact.h"
 */
class NpcFact {
 public:
  /**
   * @brief default constructor
   */
  NpcFact();
  /**
   * @brief default Destructor
   */
  virtual ~NpcFact();
   /**
   * @breif create the specified Npc type
   * @return pointer to Npc object
   */
  Npc* CreateNpc(NPCType type);
};
#endif // NPCFACT_H_INCLUDED
